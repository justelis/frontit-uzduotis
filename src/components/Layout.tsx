import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

interface Props {
    children: React.ReactNode
}

const useStyles = makeStyles((theme) => ({
    globalContainer: {
        padding: theme.spacing(4),
        minWidth: '800px',
    },
    navContainer: {
        justifyContent: 'flex-end',
        margin: theme.spacing(4, 0),
    },
    navButton: {
        'text-transform': 'none',
        'font-weight': '600',
        'margin-left': `${theme.spacing(10)}px`,
    },
    activeNavButton: {
        extend: 'navButton',
        '& .MuiButton-label': {
            flexDirection: 'column',
        },
        '& div': {
            backgroundColor: 'black',
            height: theme.spacing(0.5),
            width: '100%',
        }
    }
}));

function Layout(props: Props) {

    const classes = useStyles();

    return (
        <Container maxWidth={'xl'} className={classes.globalContainer}>
            <Grid container className={classes.navContainer}>
                <Button size={'large'} className={classes.navButton}>
                    <Typography variant={'h6'}>
                        Services
                    </Typography>
                </Button>
                <Button size={'large'} className={classes.navButton}>
                    <Typography variant={'h6'}>
                        Clients
                    </Typography>
                </Button>
                <Button size={'large'} className={classes.navButton}>
                    <Typography variant={'h6'}>
                        About us
                    </Typography>
                </Button>
                <Button size={'large'} className={classes.activeNavButton}>
                    <Typography variant={'h6'}>
                        Members
                    </Typography>
                    <div />
                </Button>
                <Button size={'large'} className={classes.navButton}>
                    <Typography variant={'h6'}>
                        Contacts
                    </Typography>
                </Button>
            </Grid>
            {props.children}
        </Container>
    )
}

export default Layout;