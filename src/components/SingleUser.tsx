import {RouteComponentProps, Link } from "react-router-dom";

type TParams = { userId: string };

function SingleUser({ match }: RouteComponentProps<TParams>) {

  console.log(match)

  return (
    <div>
      This is a page for user with ID: {match.params.userId}
      <li>
        <Link to="/">Go Back</Link>
      </li>
    </div>
  );
}

export default SingleUser;