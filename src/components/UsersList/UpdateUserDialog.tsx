import React, { Dispatch, SetStateAction, useEffect, useState } from "react"; import DialogContent from "@material-ui/core/DialogContent";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import { User } from "../../store/user/models/User";
import { AppActions } from "../../store/models/actions";
import { AppState } from "../../store/rootStore";
import { ThunkDispatch } from "redux-thunk";
import { bindActionCreators } from "redux";
import { updateUser } from '../../store/user/UserAction';
import { connect } from "react-redux";
import _ from 'lodash';

interface updateNewUserDialogProps {
  isOpen: boolean,
  setIsOpen: Dispatch<SetStateAction<boolean>>,
  idToUpdate: number,
}

interface LinkStateProps {
  users: User[];
}

interface LinkDispatchProps {
  updateUser: (user: User) => AppActions;
}

type LinkProps = updateNewUserDialogProps & LinkStateProps & LinkDispatchProps

const mapStateToProps = (state: AppState): LinkStateProps => ({
  users: state.userReducer.users
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AppActions>) => ({
  updateUser: bindActionCreators(updateUser, dispatch)
})

function UpdateNewUserDialog({ isOpen, setIsOpen, users, updateUser, idToUpdate }: LinkProps) {

  const user = _.find(users, (user) => user.id === idToUpdate);

  const [name, setName] = useState<string>(user?.name || '');
  const [email, setEmail] = useState<string>(user?.email || '');
  const [phone, setPhone] = useState<string>(user?.phone || '');
  const [website, setWebsite] = useState<string>(user?.website || '');

  useEffect(() => {
    setName(user?.name || '')
    setEmail(user?.email || '')
    setPhone(user?.phone || '')
    setWebsite(user?.website || '')
  }, [idToUpdate, user])

  const handleClose = () => {
    setIsOpen(false);
  }

  const updateNewUser = () => {
    updateUser({
      id: idToUpdate,
      name,
      email,
      phone,
      website,
    })
    handleClose();
  }


  return (
    <Dialog open={isOpen} onClose={handleClose} aria-labelledby="update-user-dialog">
      <DialogTitle>Update User</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          fullWidth
        />
        <TextField
          margin="dense"
          id="email"
          label="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          type="email"
          fullWidth
        />
        <TextField
          margin="dense"
          id="phone"
          label="Phone number"
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
          fullWidth
        />
        <TextField
          margin="dense"
          id="website"
          label="Website"
          value={website}
          onChange={(e) => setWebsite(e.target.value)}
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        <Button onClick={updateNewUser} color="primary">
          Update
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateNewUserDialog);