import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles((theme) => ({
    titleContainer: {
        alignItems: 'center',
        margin: theme.spacing(8, 0),
    },
    title: {
        'font-weight': '600',
    },
    circle: {
        width: theme.spacing(12),
        height: theme.spacing(12),
        backgroundColor: theme.palette.primary.main,
        borderRadius: '100%',
        marginRight: theme.spacing(4),
    },
}))

function Title() {

    const classes = useStyles();

    return (
        <Grid container className={classes.titleContainer}>
            <div className={classes.circle} />
            <Typography variant={'h3'} component={'h1'} className={classes.title}>
                Members
            </Typography>
        </Grid>
    )
}

export default Title;