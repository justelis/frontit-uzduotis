import React, { useEffect, useState } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ThunkDispatch } from 'redux-thunk';

import { AppState } from '../../store/rootStore';
import { AppActions } from '../../store/models/actions';

import { User } from '../../store/user/models/User';
import { boundRequestUsers, removeUser } from '../../store/user/UserAction';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import makeStyles from '@material-ui/core/styles/makeStyles';
import EditIcon from '@material-ui/icons/Edit';
import ClearIcon from '@material-ui/icons/Clear';
import TableContainer from '@material-ui/core/TableContainer';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import Title from './Title';
import TableOptions from './TableOptions';
import TableSortLabel from '@material-ui/core/TableSortLabel';

import _ from 'lodash';
import { Paper } from '@material-ui/core';
import UpdateUserDialog from './UpdateUserDialog';

interface Props { } // children: React.ReactNode

interface LinkStateProps {
    users: User[];
}

interface LinkDispatchProps {
    boundRequestUsers: () => void;
    removeUser: (id: number) => AppActions;
}

type LinkProps = Props & LinkStateProps & LinkDispatchProps

const mapStateToProps = (state: AppState): LinkStateProps => ({
    users: state.userReducer.users
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AppActions>) => ({
    boundRequestUsers: bindActionCreators(boundRequestUsers, dispatch),
    removeUser: bindActionCreators(removeUser, dispatch)
})

const useStyles = makeStyles((theme) => ({
    tableCell: {
        'font-weight': '400',
    },
}))

function UsersList(props: LinkProps) {

    const classes = useStyles();

    const [users, setUsers] = useState<User[]>([]);
    const [orderBy, setOrderBy] = useState<'name' | 'phone' | undefined>(undefined);
    const [order, setOrder] = useState<'desc' | 'asc' | undefined>(undefined);

    const [searchValue, setSearchValue] = useState<string>('');

    const [isUpdateDialogOpen, setIsUpdateDialogOpen] = useState<boolean>(false);
    const [idToUpdate, setIdToUpdate] = useState<number>(0);

    const requestUsers = props.boundRequestUsers;
    const removeUser = props.removeUser;
    const usersFromDb = props.users;

    useEffect(() => {
        requestUsers();
    }, [requestUsers])

    useEffect(() => {
        setUsers(_.filter(_.orderBy(usersFromDb, orderBy, order), function (item) {
            return item.name.includes(searchValue)
                || item.email.includes(searchValue)
                || item.phone.includes(searchValue)
                || item.website.includes(searchValue);
        }));
    }, [orderBy, order, usersFromDb, searchValue])

    const handleSortClick = (newOrderBy: 'phone' | 'name') => {
        newOrderBy === orderBy ?
            setOrder(order === 'asc' ? 'desc' : 'asc')
            :
            setOrder('asc')
        setOrderBy(newOrderBy);
    };

    let userList: JSX.Element[] = [];

    users.forEach((user: User) => {
        userList.push(
            <TableRow key={user.id}>
                <TableCell>
                    <Typography variant={'h6'} className={classes.tableCell}>
                        {user.name}
                    </Typography>
                </TableCell>
                <TableCell>
                    <Typography variant={'h6'} className={classes.tableCell}>
                        {user.email}
                    </Typography>
                </TableCell>
                <TableCell>
                    <Typography variant={'h6'} className={classes.tableCell}>
                        {user.phone}
                    </Typography>
                </TableCell>
                <TableCell>
                    <Typography variant={'h6'} className={classes.tableCell}>
                        {user.website}
                    </Typography>
                </TableCell>
                <TableCell align={'right'}>
                    <IconButton>
                        <EditIcon
                            onClick={() => {
                                setIdToUpdate(user.id);
                                setIsUpdateDialogOpen(true);
                            }}
                        />
                    </IconButton>
                    <IconButton color={'primary'} onClick={() => removeUser(user.id)}>
                        <ClearIcon />
                    </IconButton>
                </TableCell>
            </TableRow>
        )
    })

    return (
        <div>
            <UpdateUserDialog
                isOpen={isUpdateDialogOpen}
                setIsOpen={setIsUpdateDialogOpen}
                idToUpdate={idToUpdate}
            />
            <Title />
            <TableOptions searchValue={searchValue} setSearchValue={setSearchValue} />
            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                <TableSortLabel
                                    active={orderBy === 'name'}
                                    direction={orderBy === 'name' ? order : 'asc'}
                                    onClick={() => handleSortClick('name')}
                                >
                                    <Typography variant={'h6'}>
                                        Name
                                    </Typography>
                                </TableSortLabel>
                            </TableCell>
                            <TableCell>
                                <Typography variant={'h6'}>
                                    Email
                                </Typography>
                            </TableCell>
                            <TableCell>
                                <TableSortLabel
                                    active={orderBy === 'phone'}
                                    direction={orderBy === 'phone' ? order : 'asc'}
                                    onClick={() => handleSortClick('phone')}
                                >
                                    <Typography variant={'h6'}>
                                        Phone
                                    </Typography>
                                </TableSortLabel>
                            </TableCell>
                            <TableCell>
                                <Typography variant={'h6'}>
                                    Website
                                </Typography>
                            </TableCell>
                            <TableCell>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {userList}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);