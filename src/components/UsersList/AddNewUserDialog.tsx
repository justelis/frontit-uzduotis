import DialogContent from "@material-ui/core/DialogContent";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import React, { Dispatch, SetStateAction, useState } from "react";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import { User } from "../../store/user/models/User";
import { AppActions } from "../../store/models/actions";
import { AppState } from "../../store/rootStore";
import { ThunkDispatch } from "redux-thunk";
import { bindActionCreators } from "redux";
import { addUser } from '../../store/user/UserAction';
import { connect } from "react-redux";

interface AddNewUserDialogProps {
  isOpen: boolean,
  setIsOpen: Dispatch<SetStateAction<boolean>>,
}

interface LinkStateProps {
  users: User[];
}

interface LinkDispatchProps {
  addUser: (user: User) => AppActions;
}

type LinkProps = AddNewUserDialogProps & LinkStateProps & LinkDispatchProps

const mapStateToProps = (state: AppState): LinkStateProps => ({
  users: state.userReducer.users
})

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AppActions>) => ({
  addUser: bindActionCreators(addUser, dispatch)
})

function AddNewUserDialog({ isOpen, setIsOpen, users, addUser }: LinkProps) {

  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [website, setWebsite] = useState<string>('');

  const handleClose = () => {
    setIsOpen(false);
  }

  const addNewUser = () => {
    addUser({
      id: users.length + 1,
      name,
      email,
      phone,
      website,
    })
    handleClose();
  }


  return (
    <Dialog open={isOpen} onClose={handleClose} aria-labelledby="add-user-dialog">
      <DialogTitle>Add New User</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Please fill in the details to add a new user.
        </DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          fullWidth
        />
        <TextField
          margin="dense"
          id="email"
          label="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          type="email"
          fullWidth
        />
        <TextField
          margin="dense"
          id="phone"
          label="Phone number"
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
          fullWidth
        />
        <TextField
          margin="dense"
          id="website"
          label="Website"
          value={website}
          onChange={(e) => setWebsite(e.target.value)}
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        <Button onClick={addNewUser} color="primary">
          Add
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(AddNewUserDialog);