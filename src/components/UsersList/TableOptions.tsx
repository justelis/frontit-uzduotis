import React, { Dispatch, SetStateAction, useState } from "react";
import Grid from '@material-ui/core/Grid';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import AddNewUserDialog from "./AddNewUserDialog";

interface TableOptionsProps {
    searchValue: string,
    setSearchValue: Dispatch<SetStateAction<string>>,
}

const useStyles = makeStyles((theme) => ({
    tableOptionsContainer: {
        justifyContent: 'space-between',
        margin: theme.spacing(8, 0, 4, 0),
    },
    searchInput: {
        borderRadius: '10%/50%',
        fontSize: '18px',
        fontWeight: 600,
        border: '2px solid',
    },
    addNewButton: {
        'text-transform': 'none',
        borderRadius: '15%/50%',
        fontSize: '18px',
        minWidth: theme.spacing(24),
    },
}))

function TableOptions(props: TableOptionsProps) {

    const classes = useStyles();

    const [isDialogOpen, setIsDialogOpen] = useState<boolean>(false);

    return (
        <>
            <AddNewUserDialog isOpen={isDialogOpen} setIsOpen={setIsDialogOpen} />
            <Grid container className={classes.tableOptionsContainer}>
                <TextField
                    variant={'outlined'}
                    size={'small'}
                    placeholder={'Search'}
                    value={props.searchValue}
                    onChange={(e) => props.setSearchValue(e.target.value)}
                    InputProps={{
                        className: classes.searchInput,
                        endAdornment: (
                            <IconButton>
                                <SearchIcon />
                            </IconButton>
                        ),
                    }}
                />
                <Button
                    variant={'contained'}
                    className={classes.addNewButton}
                    size={'large'}
                    color={'primary'}
                    onClick={() => setIsDialogOpen(true)}
                >
                    Add New
                </Button>
            </Grid>
        </>
    )
}

export default TableOptions;