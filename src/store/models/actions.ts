import { UserActionTypes } from '../user/models/actions';

export type AppActions = UserActionTypes;