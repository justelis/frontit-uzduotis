import { UserActionTypes, FETCH_USERS_REQUEST, FETCH_USERS_SUCCESS, FETCH_USERS_FAILURE, REMOVE_USER, ADD_USER, UPDATE_USER } from './models/actions';
import { User } from './models/User';
import _ from 'lodash';

interface UserState{
    isLoading: boolean,
    users: User[],
    error: string,
}

const defaultState: UserState = {
    isLoading: false,
    users: [],
    error: '',
}

export const userReducer = (state = defaultState, action: UserActionTypes): UserState => {
    switch (action.type) {
        case FETCH_USERS_REQUEST:
            return { isLoading: true, users: [], error: '' }
        case FETCH_USERS_SUCCESS:
            return { isLoading: false, users: action.users, error: '' }
        case FETCH_USERS_FAILURE:
            return { isLoading: false, users: [], error: action.error }

        case REMOVE_USER:
            return { isLoading: false, users: _.filter(state.users, function(user) {return user.id !== action.id}), error: '' }
        case ADD_USER:
            return { isLoading: false, users: _.concat(state.users, action.user), error: '' }
        case UPDATE_USER:
            return { 
                isLoading: false, 
                users: _.map(state.users, function(user) { return user.id === action.user.id ? action.user : user}), 
                error: '' 
            }
        default:
            return state;
    }
}