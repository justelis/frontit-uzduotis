import { User } from "./User";

export const FETCH_USERS_REQUEST = 'FETCH_USERS_REQUEST';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_RSUCCESS';
export const FETCH_USERS_FAILURE = 'FETCH_USERS_FAILURE';

export const REMOVE_USER = 'REMOVE_USER';
export const ADD_USER = 'ADD_USER';
export const UPDATE_USER = 'UPDATE_USER';

interface UserAsync {
    isLoading: boolean;
    users: User[];
    error: string;
}

interface FetchUsersRequest extends UserAsync {
    type: typeof FETCH_USERS_REQUEST;
}

interface FetchUsersSuccess extends UserAsync {
    type: typeof FETCH_USERS_SUCCESS;
}

interface FetchUsersFailure extends UserAsync {
    type: typeof FETCH_USERS_FAILURE;
}

interface RemoveUser {
    type: typeof REMOVE_USER,
    id: number
}

interface AddUser {
    type: typeof ADD_USER,
    user: User,
}

interface UpdateUser {
    type: typeof UPDATE_USER,
    user: User,
}

export type UserActionTypes = FetchUsersRequest | FetchUsersSuccess | FetchUsersFailure | RemoveUser | AddUser | UpdateUser;