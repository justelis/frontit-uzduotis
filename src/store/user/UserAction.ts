import { Dispatch } from "redux";
import { AppActions } from '../models/actions';
import { FETCH_USERS_REQUEST, FETCH_USERS_SUCCESS, FETCH_USERS_FAILURE, REMOVE_USER,ADD_USER, UPDATE_USER } from './models/actions';
import { User } from './models/User';

const  requestUsers = (): AppActions => ({
    type: FETCH_USERS_REQUEST,
    isLoading: true,
    users: [],
    error: '',
})

const  receiveUsers = (users: User[]): AppActions => ({
    type: FETCH_USERS_SUCCESS,
    isLoading: false,
    users,
    error: '',
})

const  invalidateUsers = (users: User[]): AppActions => ({
    type: FETCH_USERS_FAILURE,
    isLoading: false,
    users: [],
    error: 'Unable to fetch User list',
})

const removeUser = (id: number): AppActions => ({
    type: REMOVE_USER,
    id,
})

const addUser = (user: User): AppActions => ({
    type: ADD_USER,
    user,
})

const updateUser = (user: User): AppActions => ({
    type: UPDATE_USER,
    user,
})

// https://jsonplaceholder.typicode.com/users

const boundRequestUsers = () => {
    return (dispatch: Dispatch<AppActions>) => {
        dispatch(requestUsers());
        return fetch(`https://jsonplaceholder.typicode.com/users`)
            .then((res) => res.json())
            .then((jsonData) => dispatch(receiveUsers(jsonData)))
    }
}

export {boundRequestUsers, removeUser, addUser, updateUser}