import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import UsersList from './components/UsersList';
import SingleUser from './components/SingleUser';

import { create } from 'jss';
import { StylesProvider, jssPreset, ThemeProvider } from '@material-ui/core/styles';
import extendPlugin from 'jss-plugin-extend';
import Layout from './components/Layout';

import { createTheme } from '@material-ui/core/styles';
import orange from '@material-ui/core/colors/orange';
import red from '@material-ui/core/colors/red';

const jss = create({
  plugins: [...jssPreset().plugins, extendPlugin()],
});

const theme = createTheme({
  palette: {
    primary: {
      main: orange[500],
    },
    secondary: {
      main: red[500],
    },
  },
});

const App = () => {
  return (
    <StylesProvider jss={jss}>
      <ThemeProvider theme={theme}>
        <Router>
          <Layout>
            <Route path="/" exact component={UsersList} />
            <Route path="/:userId" component={SingleUser} />
          </Layout>
        </Router>
      </ThemeProvider>
    </StylesProvider>
  );
}

export default App;
